package gitlab

type Event struct {
	Kind              string `json:"object_kind"`
	Ref               string `json:"ref"`
	User_Name         string `json:"user_name"`
	Username          string `json:"user_username"`
	CheckoutSha       string `json:"checkout_sha"`
	TotalCommitsCount int    `json:"total_commits_count"`
	Project           Project
}
