package gitlab

type Project struct {
	ID            int    `json:"id"`
	Name          string `json:"name"`
	WebURL        string `json:"web_url"`
	GitSSHURL     string `json:"git_ssh_url"`
	GitHTTPURL    string `json:"git_http_url"`
	Namespace     string `json:"namespace"`
	DefaultBranch string `json:"default_branch"`
}
