package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.GET("/gitlab", gitlab)
	e.Logger.Fatal(e.Start(":8043"))
}

func gitlab(c echo.Context) error {
	switch c.Request().Header["X-Gitlab-Event"] {
	case "Push Hook":
		gitlab.OnPush(c.Request())
	}
}
